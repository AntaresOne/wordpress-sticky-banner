=== Sticky banner ===

Contributors: hiddendepth
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=22P45UAZ84JQL&source=url
Tags: banner, banners, announcement, announcements, cta, notification, fixed, sticky, notice, free
Requires at least: 4.7
Tested up to: 5.6
Requires PHP: 5.6
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display a sticky banner/bar at the top or bottom of your website.

== Description ==

This plugin makes it easy to display a sticky announcement banner, at the top or bottom of your website. You can customise the colours to suit your site or brand. Preview your banner from the settings page, before you save/update.

== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins > Add New'
2. Search for 'Sticky Banner'
3. Activate 'Sticky Banner' from your Plugins page.
4. Visit 'Sticky Banner' in the sidebarner to create a new banner.

= From WordPress.org =

1. Download 'Sticky Banner'.
2. Upload the 'hdsb-stickybanner' directory to your '/wp-content/plugins/' directory, using your favorite method (ftp, sftp, scp, etc...)
3. Activate 'Sticky Banner' from your Plugins page.
4. Visit 'Sticky Banner' in the sidebar to add content and change colours.

== Frequently Asked Questions ==

= Will the banner hide my content =
The banner automatically detects its own height and applies padding to the top or bottom of the page to make sure no content is hidden. 
If you have other fixed elements on your page (like a menu bar or header) that sticks to the top/bottom of the page - you can override default z-index and padding by using CSS to target class hdsb-t-active or hdsb-b-active. 

= What font does the banner use =
The banner font is Arial, which is universal font, available on all devices.


== Screenshots ==

== Changelog ==

= 1.1.0 =
The plugin has been coded from the ground up to be cleaner and make future updates easier.

-   New setting: Cookie expiry days option added to settings
-   New setting: Hide on pages option added (add page IDs with commas)
-   New: Adds an active class to the body for styling (hdsb-t-active or hdsb-t-active)
-   New: Users can now close banner with X on right hand side (adds cookie for 7 days)
-   Update: Button will not show without a URL added to settings

= 1.0.0 =

-   Initial Version.

== Upgrade Notice ==

= 1.0.0 =
This is the first version.
